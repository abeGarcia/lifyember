//$(document).foundation();

window.Lify = Ember.Application.create({
    rootElement: "#main-container"
});

Lify.Router.map(function() {
  this.route("signup");
  this.route("login");
  this.resource("memories");
  this.resource("memory", function(){
    this.route("addmemory");
  });
});

Lify.AuthenticatedRoute = Ember.Route.extend({
    beforeModel: function(transition) {
        if (!this.controllerFor('login').get('token')) {
          this.redirectToLogin(transition);
        }
    },
    redirectToLogin: function(transition) {
        alert('you must log in!');
        var loginController = this.controllerFor('login');
        loginController.set('attemptedTransition', transition);
        this.transitionTo('login');
    },
    getwithToken : function(url){
        var token = this.controllerFor("login").get("token");
        return Ember.$.getJSON(url, { token: token });
    },
    actions: {
        error: function(error, transition) {
          if (error.status === 401) {
            this.redirectToLogin(transition);
          } else {
            alert('Something went wrong');
          }
        }
    }
})

Lify.SignupController = Ember.Controller.extend({
    actions : {
        sign_up: function(){
            var self = this;
            var data = this.getProperties("email","password");
            var url = "http://lifyuat.codelikeasir.com/User/Register";
            Ember.$.post(url, { Email: data.email, Password: data.password }, function(result){
                self.transitionToRoute("login");
            });
        }
    }
});

Lify.LoginController = Ember.Controller.extend({
    token: localStorage.token,
    tokenChanged: function() {
        localStorage.token = this.get('token');
    }.observes('token'),
    actions : {
        log_in: function(){
            var self = this;
            var data = this.getProperties("email","password");
            var url = "http://lifyuat.codelikeasir.com/User/Login";
            Ember.$.post(url, { Email: data.email, Password: data.password }, function(result){
                if (!result.Number){
                    var attemptedTransition = self.get('attemptedTransition');
                    if (attemptedTransition) {
                      attemptedTransition.retry();
                      self.set("token", result.APIKey);
                    } else {
                      self.transitionToRoute("memories");
                    }
                }
            });
        }
    }
});

Lify.MemoriesRoute = Lify.AuthenticatedRoute.extend({
   model: function(){
    return this.getwithToken("http://lifyuat.codelikeasir.com/Memory/GetMyMemories");
   }
});

Lify.MemoriesController = Ember.ArrayController.extend({
    sortProperties: ['Name'],
    needs: ["login"],
    user: Ember.computed.alias("controllers.login"),
    actions: {
        delete: function(item){
            var self = this;
            //{ IDMemory: $("#Data1").val(), token: getToken() };
            var token = this.get("user").get("token");
            var url = "http://lifyuat.codelikeasir.com/Memory/Delete";
            Ember.$.post(url, { IDMemory: item.ID, token: token }, function(result){
                //removeObject locally
                self.get("model").removeObject(item);
            });
        }

    }
});


Lify.MemoryAddmemoryController = Ember.Controller.extend({
    isPublic:1,
    needs: ["login"],
    user: Ember.computed.alias("controllers.login"),
    actions : {
        create: function(){
            var self = this;
            var token = this.get("user").get("token");
            var data = this.getProperties("name", "expire");
            var url = "http://lifyuat.codelikeasir.com/Memory/Create";
            Ember.$.post(url, { Name: data.name, IsPublic: String(this.isPublic), Publish: "2014-04-21 19:50:54.0000000", Expire: "2014-04-21 19:50:54.0000000", Locked: "1", token: token }, function(result){
                //reset form
                self.set("name", "");
                self.set("expire", "");
                self.transitionToRoute("memories");
            });
        }
    }
});

//RADIO BTNS
Ember.RadioButton = Ember.View.extend({ 
    tagName : "input",
    type : "radio",
    attributeBindings: [ "name", "type", "value", "checked:checked:" ],
    click : function(){
        this.set("selection", this.$().val())
    },
    checked : function() {
        return this.get("value") == this.get("selection");   
    }.property()

});

//HELPERS
Ember.Handlebars.registerBoundHelper('date', function (date) {
    var start = date.indexOf('(');
    var startLength = date.substr(start+1).length
    var end = date.substr(date.indexOf(')'));
    var currentDate = date.substr(start+1, startLength-end.length);
    return moment(new Date(parseInt(currentDate))).fromNow();
});

